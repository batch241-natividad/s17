
/*
Functions
	Functions in javascript are lines/ block of codes that tell our device
	/application to perform a certain task when called or what we called the
	invoked.

	Functions are mostly created to create complicated tasks to run several
	lines of code in succession.
	
	function are also used to prevent repeating lines or blocks of codes that
	perform the same task/function.

	*/

//Function Declaration

	//(function statement) defines a function with the specified paramaters 


/*

	Syntax:
		function functionName(){
	
			codeblock(statement)
		}

		function keyword used to defined a javascript functions
		functionName - the function name.Function are named to be able to use
		later in the code.

		function block ({}) - the statements which comprise the body of the functionl.
		This is where the code to be executed.
		*/

		console.log("My name is JOhn Outside!");

		function printName() {

			console.log("My name is John!");
		}

//Function Invocation
printName();


/*
	The code block and statements inside a function is not immediately executed whehn the function
	is defined. The code block and statements inside a function is executed when the function is invoked or called.


	*/
//Invoke/Call the function that we declared


// declaredFunction();

// Function Declaration vs Function Expressions

	//Function Declaration
	//A function can be created through function declaration by using the
	 // function keyword and adding the function name.
	 // Declared function are not executed immediately. They are "saved for later used" and will be executed later when they are invoked(called).

	 declaredFunction();


	 function declaredFunction() {
	 	console.log("Hello World from the declaredFunction()");

	 }

	 declaredFunction();

	//Function Expressions
		// A fucntion can also be stored in a variable. This is what we called a function expression

		//A function expression is an anonymous function assigned to the variableFunction.

		// Anonymous function - a function without a name 

		let variableFunction = function(){

			console.log("hello from the variableFunction");

		}


		variableFunction(); 

//Function expression are ALWAYS invoked(called) using the variable name.


let functionExpression = function funcName() {

	console.log("hello from the other side");

}

functionExpression();
	// funcName();

	//Can we reassign declared functions and function expression to a NEW anonymous functions? yes!

	declaredFunction = function() {

		console.log("Updated declaredFunction");


	}

	declaredFunction();

	functionExpression = function () {
		console.log("Updated functionExpression");
	}


	functionExpression();

	//We cannot re-assigned a function expression that is initialized with const.

	// const constantFunc = function() {

	// 	console.log("Initialized with CONST");

	// 	constantFunc();
	// }

	// constantFunc = function() {

	// 	console.log("Can we reassigned?");

	// }

	// constantFunc();

	//Function scoping

	/*

		Scope is the accessibility (visibility)  of variables
		JS Variables has 3 types of scope
			1. local/block scope
			2. global scope
			3. function scope

			*/

			{

				let localVar = "I am a local variable."
			}

			let globalVar = "I am a global variable."


			console.log(globalVar);

	// console.log(localVar);

	function showNames () {

		//Function scoped variables
		var functionVar = "Joe";
		const functionConst = "John";
		let functionLet = "Jane";

		console.log(functionVar);
		console.log(functionConst);
		console.log(functionLet);
	}


	showNames();

		//The variables, functionvar,functionConst, and functionlet are function scoped variables. They can only be accessed inside of the function they were declared in.


		// console.log(functionVar);
		// console.log(functionConst);
		// console.log(functionLet);


	//NESTERD FUNCTION
		// You can create another function inside a function. This is called a nested function. This nested function, being inside the myNewFunc will have access to the variable, "name" as they are within the same code block/scope
		function myNewFunction(){

			let name = "Maria";

			function nestedFunction() {
				let nestedName = "Jose";
				console.log(name);
			}


			nestedFunction();

		}

		myNewFunction();
		//nestedFunction(); this will cause an error.


//Function and Global scoped variables

	//Global Variable
	// let globalName = "Erven Joshua";

	// function myNewFunction2(){

	// 	let nameInside = "Jenno";

	// 	console.log(globalName);
	// }

	// myNewFunction2();

	// console.log(nameInside);

//USING ALERT()
/*
	Syntax:
		alert(message)


*/


// alert("HELLO WORLD!");

function showSampleAlert() {
	alert("hello uUser!")

}

showSampleAlert();

console.log("I will only log in the console when the alert is dismiss!");


//USING PROMPT()
/*
	syntax:
		prompt("<DialogueInString>");

*/

// 	let samplePrompt = prompt("Enter your name:");



// 	console.log("Hello, " + samplePrompt);

// console.log(typeof detail);



	function printWelcomeMessage() {

		let firstName = prompt("Enter your First Name:");
		let lastName = prompt("Enter your last name:");

		// console.log("Hello" + firstName + ' ' + lastName + "!");
		// console.log("Welcome to my page!");

	alert("Hello" + firstName + ' ' + lastName + "!");
		 alert("Welcome to my page!");


	}
	printWelcomeMessage()


	//Function Naming Conventions

		//Name your functions in small caps. Follows camelCase when naming variables and functions

		function displayCarInfo() {
			console.log("Brand: Toyota");
			console.log("Type: Sedan");
			console.log("Price: 1, 500, 000");


		}


		displayCarInfo();

		//Function names should be definitive of the task it will perform. It usually contains a verb.

		function getCourse(){
			let courses = ["Science 101", "Math 101","English 101"];
			console.log(courses);
		}

		getCourse();

		//Avoid generic names to avoid confusion within your code 


		//Better kung getName
		function get() {
			let name = "Jamie";
			console.log(name);
		}

		get();

		//Avoid pointless and inappropriate function names
		function foo(){
			console.log (25 % 5);
		}

		foo();
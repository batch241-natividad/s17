/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
		*/
		
	//first function here:

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
		*/

	//second function here:

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
		*/
		
	//third function here:

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
		*/


		function printUsers(){
			
			let printFullName = prompt("What is your name:"); 
			let printAge = prompt("How old are you:"); 
			let printLocation = prompt("Where do you live:");
			alert("Thank you for your input");
			
			
			console.log("Hello, " + printFullName) 
			console.log("You are " + printAge +" years old."); 
			console.log("You live in " + printLocation); 
		};

		printUsers();



		function displayFavoriteArtists() {
			console.log("1. Taylor Swift");
			console.log("2. Clara Benin");
			console.log("3. Adie ");
			console.log("4. kinokoteikoku ");
			console.log("5. Lilypichu ");


		}


		displayFavoriteArtists();


		function displayFavoriteMovies() {
			console.log("1.3 idiots");
			console.log("Rotten Tomatoes Rating: 100%");
			console.log("2.Your Name");
			console.log("Rotten Tomatoes Rating: 98%");
			console.log("3.Spider-Man-No Way Home ");
			console.log("Rotten Tomatoes Rating: 93%");			
			console.log("4.Doctor Strange in the multiverse of madness");
			console.log("Rotten Tomatoes Rating: 74%");
			console.log("5.Coraline");
			console.log("Rotten Tomatoes Rating: 90%");


		}


		displayFavoriteMovies();
		

		alert("Hi! Please add the names of your friends.");

		function printFriends(){

			let friend1 = prompt("Enter your first friend's name:"); 
			let friend2 = prompt("Enter your second friend's name:"); 
			let friend3 = prompt("Enter your third friend's name:");

			console.log("You are friends with:")
			console.log(friend1); 
			console.log(friend2); 
			console.log(friend3); 
		};
		printFriends();
